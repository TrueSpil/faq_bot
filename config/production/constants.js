module.exports = {

    /************************    ROLES OF USERS    ******************************/
    ROLE_NOT_AUTH: 0,
    ROLE_IS_AUTH: 1,
    ROLE_IS_MANAGER: 2,
    ROLE_IS_ADMIN: 3,
    ROLE_IS_SYSTEM: 4
};