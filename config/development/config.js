module.exports = {
    appName: 'faq_bot_dev',
    access_control: {
        domains: '*',
        maxAge: 360000,
    },
    port: 8096,
    access: {
        role: 0
    },
    connections: {
        MYSQL: {
            master:{
                "host": "127.0.0.1",
                "port": 8889,
                "username": "root",
                "password": "root",
                "database": "airdrop"
            },
            slaves:[]
        }
    },
    TELEGRAM_BOT: {
        token: '844011723:AAHWA1UySgReCG_2J3PRzdYnqtiilgtPvzI',
        limit: 300,
        polling_timeout: 0,
        sync_interval: 1000,
        retry_timeout: 5000,
        timer: 30000
    }
};