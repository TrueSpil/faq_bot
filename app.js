"use strict";
global.ENV = process.env.NODE_ENV || 'production';

global.__dirroot = __dirname;
global.__dirapp = __dirname + '/app/';
global.__dirlogger = __dirname + '/logger/';
global.__dirconfig = __dirname + '/config/' + ENV + '/';

global.__dircore = __dirapp + 'core/';
global.__dirhelper = __dirapp + 'helpers/';
global.__dirrouter = __dirapp + 'router/';
global.__dirlibs = __dirapp + 'libs/';
global.__dirservices = __dirapp + 'services/';

global.__dircash = __dircore + 'cash/';
global.__dirmodels = __dircore + 'models/';
global.__dircontrollers = __dircore + 'controllers/';
global.__dirworkers = __dirapp + 'workers/';


//globals lib
global._ = require('underscore');
global.fs = require('fs');
global.log = require(__dirlibs + 'log');
//App
require(__dirrouter + 'router');
global.Storage = require(__dirlibs + 'storage');
// require(__dirlibs + 'storage');
require(__dirworkers + 'telegram_bot');

console.log(Storage);
Storage.init();

/** -----------------------------------------------------
 *  Error catcher
 */
process.on('uncaughtException', function (err) {
    log.e(err);
    if (err.message.indexOf('EMFILE') != -1 || err.message.indexOf('EADDRINUSE') != -1) {
        process.exit(1);
    }
});