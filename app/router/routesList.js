const extend = require('util')._extend;
const fs = require('fs');

const routes_dir = `${__dirname}/routeLists/`;
const items = fs.readdirSync(routes_dir);

if (!items.length) {
  return log.e('CAN`T READ routes', items);
}

let routes = {};
items.forEach(item => {
  extend(routes, require(routes_dir + item));
});

module.exports = routes;