/**
 * Created by victor on 5/6/16.
 */
"use strict";
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const helmet = require('helmet');

/** ----------------------------------------------------
 *  SSL
 */
const express = require('express');
const app = express();


let server = require('http').createServer(app);


const request = require('request');

/** ----------------------------------------------------
 *
 */
const cfg = require(__dirconfig + 'config');
const constants = require(__dirconfig + 'constants');

/** ----------------------------------------------------
 *
 */

/** ----------------------------------------------------
 *
 */
// const routesList = require('./routesList');
// const RestRoutes = require('./restRoutes');

/** ----------------------------------------------------
 * REST middleware
 */

app.use(helmet());

app.use(cookieParser());
app.use(bodyParser.json({limit: '12mb'}));
app.use(bodyParser.urlencoded({limit: "12mb", extended: true, parameterLimit: 12000}));
// app.use(new RestRoutes(routesList));


/** ----------------------------------------------------
 *
 */
server.listen(cfg.port);
log.i('Server started on', cfg.port);