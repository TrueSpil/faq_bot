const TelegramBot = require('telebot');
const cfg = require(__dirconfig + 'config');
const iqfinexRest = require(__dirlibs + 'rest');
const search = require(__dirlibs + 'searching');


const bot = new TelegramBot({
    token: cfg.TELEGRAM_BOT.token, // Required. Telegram Bot API token.
    usePlugins: ['commandButton'],

    polling: { // Optional. Use polling.
        interval: cfg.TELEGRAM_BOT.sync_interval, // Optional. How often check updates (in ms).
        timeout: cfg.TELEGRAM_BOT.polling_timeout, // Optional. Update polling timeout (0 - short polling).
        limit: cfg.TELEGRAM_BOT.limit, // Optional. Limits the number of updates to be retrieved.
        retryTimeout: cfg.TELEGRAM_BOT.retry_timeout // Optional. Reconnecting timeout (in ms).
    }
});



// Command /start
bot.on('/start', msg => {

    return bot.sendMessage(msg.from.id, 'Приветсвую вас. Я постараюсь ответить на ваши вопросы! Не стесняйтесь, пишите, а я попробую вам помочь');

});

// Command /hello
bot.on('/hello', msg => {
    return bot.sendMessage(msg.from.id, 'Hello!');
});
// Command /hello
bot.on('/add_question', msg => {
    return bot.sendMessage(msg.from.id, 'Hello!');
});

bot.mod('text', (msg) => {
    return msg;
});
bot.on('text', msg => {
    if (msg.text[0] === '/') {
        switch (msg.text.split('_')[0]) {
            case '/question':
                Storage.getQuestionById(Number(msg.text.split('_')[1]) - 1)
                    .then(item => {
                        if (!item) return;
                        let markdown = '';
                        let parseMode = 'html';

                        markdown += (`<b>${item.question}</b> \n\n`);
                        let answer = item.answer;
                        markdown += (`${answer} \n`);

                        return bot.sendMessage(msg.from.id, markdown, {parseMode});
                    })
                    .catch(err => {
                        log.e(err);
                    });

                break;
            default:
                break;
        }
    } else {
        search.startSearch(msg.text)
            .then(resp => {
                let markdown = '';
                let parseMode = 'html';

               if (resp.length) {
                   _.each(resp, item => {
                       markdown += (`<b>${item.obj.question}</b> \n`);
                       let answer = item.obj.answer;
                       if (answer.length >= 100) {
                           answer = answer.slice(0, 97);
                           answer += '...';
                           markdown += (`${answer} \n`);
                           markdown += (`подробнее /question_${item.obj.id + 1}\n\n`);
                       } else {
                           markdown += (`${answer} \n`);
                       }
                   });
               } else {
                   markdown += 'К сожалению я ничего не нашел по вашему вопросу'
               }

                return bot.sendMessage(msg.from.id, markdown, {parseMode});
            })
            .catch(err => {
                log.w(err);
            })
    }
});

// Button callback
bot.on('callbackQuery', (msg) => {

    console.log('callbackQuery data:', msg.data);
    bot.answerCallbackQuery(msg.id);

});

bot.start();