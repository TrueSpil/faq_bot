const fuzzysort = require('fuzzysort');


const Searching = (() => {

    return {
        cash: {},
        startSearch: (text) => {
            return new Promise((resolve, reject) => {
                Storage.getQuestions()
                    .then(data => {
                        resolve(fuzzysort.go(text.split(' ').join(''), data, {keys: ['question'], limit: 5}));
                    })
                    .catch(err => {
                        reject(err);
                    })
            } )
        }
    }
})();


module.exports = Searching;