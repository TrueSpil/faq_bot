const cfg = require(__dirconfig + 'config').connections;
const request = require('request');

function send(method, url, data) {
    return new Promise((resolve, reject) => {
        let options = {
            rejectUnauthorized: false,
            method: method,
            url: url,
            timeout: 60000,//1 min
            headers: {
                Authorization: cfg.token,
                'content-type': 'application/json',
            },
            body: JSON.stringify(data || {})
        };
        request(options, function (err, httpResponse, body) {
            if (err != null) {
                log.e("Connection error", err);
                return reject(err);
            }
            else {
                let json;
                try {
                    json = JSON.parse(body);
                }
                catch (err) {
                    log.e(err);
                    return reject("Parse error", err);
                }
                if (json.status === 200) {
                    return resolve(json.data);
                }
                return reject(json.data);
            }
        })
    });
}

const host = '';
module.exports = {
    addAirdrop(data) {
        return send('post', host + 'balance/add/' + data.user, data);
    },
    getUser(id) {
        return send('get', host + 'user/data/' + id);
    }
}