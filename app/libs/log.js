"use strict";
const chalk = require('chalk');

function _log(type, title, text) {
    if (text.length > 1) {
        text = text.join('\t');
    }
    else {
        text = text[0];
    }
    let date = new Date();
    let pre = date.getDate() + ' ' +
        date.toLocaleString("en-us", {month: "short"}) + ' ' +
        date.getHours() + ':' +
        ("0" + date.getMinutes()).slice(-2) + ':' +
        ("0" + date.getSeconds()).slice(-2) + '.' +
        ("00" + date.getMilliseconds()).slice(-3) + ' -';

    let author = ' Master';

    switch (type) {
        case 'info':
            pre += chalk.blue(author);
            break;
        case 'warn':
            pre += chalk.bold.yellow(author);
            break;
        case 'error':
            pre += chalk.bold.red(author);
            break;
        case 'critical':
            pre += chalk.bold.bgRed.black(author);
            break;
        case 'log':
            pre += chalk.bold(author);
            break;
    }
    return console[type](pre, title, text | '');
}

module.exports = {
    d(title, ...text ) {
        return _log('log', title, text);
    },
    i(title, ...text) {
        return _log('info', title, text);
    },
    w(title, ...text) {
        return _log('warn', title, text);
    },
    e(title, ...text) {
        return _log('error', title, text);
    },
    c(title, ...text) {
        return _log('critical', title, text);
    }
};



