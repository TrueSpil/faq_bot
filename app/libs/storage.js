const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
// Connection URL
const url = 'mongodb://localhost:27017';

// Database Name
const dbName = 'test';

// Use connect method to connect to the server


const Storage = (() => {
    return {
        db: null,
        init: () => {
               return Storage.connect('mongodb://localhost:27017', 'test')
                    .then(() => {
                        log.d('Base', "Connected successfully to server");
                    })

                   .catch(err => {
                       log.e(err);
                   })

        },
        connect: (url, dbName) => {
            return new Promise((resolve, reject) => {
                MongoClient.connect(url, {"useNewUrlParser": true}, function (err, client) {
                    assert.equal(null, err);
                    Storage.db = client.db(dbName);
                    resolve();
                });
            })

        },
        getQuestionById: (id) => {
               return Storage.db.collection('questions').findOne({id: Number(id)})

        },
        getQuestions: () => {
            return new Promise((resolve) => {
                Storage.db.collection('questions').find({}).toArray( (err, array) => {
                    resolve(array);
                })
            })
        }
    }
})();


module.exports = Storage;